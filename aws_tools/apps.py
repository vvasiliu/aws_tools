from django.apps import AppConfig


class AwsBackupConfig(AppConfig):
    name = 'aws_tools'
